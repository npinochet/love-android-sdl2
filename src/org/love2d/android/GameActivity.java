package org.love2d.android;

import org.love2d.android.GameActivityOriginal;

import android.util.Log;
import android.view.*;
import android.util.DisplayMetrics;
import android.os.Bundle;
import android.content.Context;

import com.google.android.gms.ads.*;

import com.google.android.gms.common.*;
import com.google.android.gms.common.api.*;
import com.google.android.gms.games.*;
import android.content.IntentSender;
import android.app.Dialog;

import android.widget.RelativeLayout;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class GameActivity extends GameActivityOriginal implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

	private DisplayMetrics metrics = new DisplayMetrics();
	private Context context;

	//admob stuff
	private AdView adView;
	private InterstitialAd mInterstitialAd;
	private AdRequest adRequest;
	private boolean adLoaded = false;
	private boolean hasBanner = false;
	private boolean hasInterstitial = false;

	//GPS stuff
	private GoogleApiClient mGoogleApiClient;
	private boolean googleConnected = false;
	private static int RC_SIGN_IN = 9001;
	private static int REQUEST_ACHIEVEMENTS = 1000;
	private static int REQUEST_LEADERBOARD = 1001;
	private static int REQUEST_ALL_LEADERBOARD = 1002;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this.getApplicationContext();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
	}

	@Override
	protected void onStart() {
		super.onStart();
		if (isGooglePlayConnected()){
			mGoogleApiClient.connect();
		}
	}


	private boolean isOnline() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}
	
	public void createBanner(final String ad_id, final String side, final String size) {
		Log.d("GameActivity", "Calling CreateBanner");

		runOnUiThread(new Runnable(){
			@Override
			public void run() { 

				if (isOnline() && hasBanner == false){
					adView = new AdView(mSingleton);
					adView.setAdUnitId(ad_id);

					if (size.trim().equals("LARGE_BANNER")) {
						adView.setAdSize(AdSize.LARGE_BANNER);
					}else if(size.trim().equals("MEDIUM_RECTANGLE")) {
						adView.setAdSize(AdSize.MEDIUM_RECTANGLE);
					}else if(size.trim().equals("FULL_BANNER")) {
						adView.setAdSize(AdSize.FULL_BANNER);
					}else if(size.trim().equals("LEADERBOARD")) {
						adView.setAdSize(AdSize.LEADERBOARD);
					}else if(size.trim().equals("SMART_BANNER")) {
						adView.setAdSize(AdSize.SMART_BANNER);
					}else{
						adView.setAdSize(AdSize.BANNER);
					};

					AdSize adSize = adView.getAdSize();

					RelativeLayout container = new RelativeLayout(mSingleton);
					RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

					params.leftMargin = (metrics.widthPixels/2) - (adSize.getWidthInPixels(context)/2);
					if (side.trim().equals("bottom")) {
						params.topMargin = metrics.heightPixels - adSize.getHeightInPixels(context);
					}

					adRequest = new AdRequest.Builder().build();
					adView.loadAd(adRequest);

					container.addView(adView, params);
					mLayout.addView(container);

					// solution to a strange bug that make the banner disapear
					adView.setAdListener(new AdListener(){
						@Override
						public void onAdLoaded() {
							adView.setVisibility(View.GONE);
							adView.setVisibility(View.VISIBLE);
						}
					});
					hasBanner = true;
					Log.d("GameActivity", "Banner Created. ID: "+ad_id);
				}
			}
		});
	}

	public void createInterstitial(final String ad_id) {
		Log.d("GameActivity", "Calling CreateInterstitial");

		runOnUiThread(new Runnable(){
			@Override
			public void run() { 

				if (isOnline() && hasInterstitial == false) {
					mInterstitialAd = new InterstitialAd(mSingleton);
					mInterstitialAd.setAdUnitId(ad_id);

					AdRequest adRequest = new AdRequest.Builder().build();
					mInterstitialAd.loadAd(adRequest);

					mInterstitialAd.setAdListener(new AdListener() {
						@Override
						public void onAdClosed() {
							AdRequest adRequest = new AdRequest.Builder().build();
							mInterstitialAd.loadAd(adRequest);
						}
					});

					hasInterstitial = true;
					Log.d("GameActivity", "Interstitial Created. ID: "+ad_id);
				}
			}
		});
	}

	public void hideBanner() {
		Log.d("GameActivity", "Calling hideBanner");

		runOnUiThread(new Runnable(){
			@Override
			public void run() { 

				if (hasBanner == true){
					adView.setVisibility(View.GONE);
					Log.d("GameActivity", "Banner Hidden");
				}
			}
		});
	}

	public void showBanner() {
		Log.d("GameActivity", "Calling showBanner");

		runOnUiThread(new Runnable(){
			@Override
			public void run() { 

				if (hasBanner == true){
					adView.loadAd(adRequest);
					adView.setVisibility(View.VISIBLE);
					Log.d("GameActivity", "Banner Showing");
				}
			}
		});
	}

	public boolean isInterstitialLoaded() {
		Log.d("GameActivity", "Calling isInterstitialLoaded");

		runOnUiThread(new Runnable(){
			@Override
			public void run() {

				if (hasInterstitial == true){
					if (mInterstitialAd.isLoaded()) {
						adLoaded = true;
						Log.d("GameActivity", "Ad is loaded");
					}else{
						adLoaded = false;
						Log.d("GameActivity", "Ad is not loaded.");
					}
				}
			}
		});
	 return adLoaded;
	}
 
	public void showInterstitial() {
		Log.d("GameActivity", "Calling showInterstitial");

		runOnUiThread(new Runnable(){
			@Override
			public void run(){

				if (hasInterstitial == true){
					if (mInterstitialAd.isLoaded()){
						mInterstitialAd.show();
						Log.d("GameActivity", "Ad loaded!, showing...");
					} else {
						Log.d("GameActivity", "Ad is NOT loaded!, skipping.");
					}
				}
			}
		});
	}

	public void googlePlayConnect(){
		if (mGoogleApiClient == null){
			mGoogleApiClient = new GoogleApiClient.Builder(this)
			.addConnectionCallbacks(this)
			.addOnConnectionFailedListener(this)
			.addApi(Games.API).addScope(Games.SCOPE_GAMES)
			.build();
		}
		if (mGoogleApiClient != null && googleConnected == false){
			mGoogleApiClient.connect();
		}
	}

	public void googlePlayDisconnect(){
		if (isGooglePlayConnected()){
			Games.signOut(mGoogleApiClient);
			mGoogleApiClient.clearDefaultAccountAndReconnect();
			googleConnected = false;
		}
	}

	public boolean isGooglePlayConnected(){
		if (isOnline()){
			return (mGoogleApiClient != null && googleConnected == true && mGoogleApiClient.isConnected());
		}
		return false;
	}

	public void unlockAchievement(String id){
		if (isGooglePlayConnected()){
			Games.Achievements.unlock(mGoogleApiClient, id);
		}
	}

	public void incrementAchievement(String id, int inc){
		if (isGooglePlayConnected()){
			Games.Achievements.increment(mGoogleApiClient, id, inc);
		}
	}

	public void showAchievements(){
		if (isGooglePlayConnected()){
			startActivityForResult(Games.Achievements.getAchievementsIntent(mGoogleApiClient), REQUEST_ACHIEVEMENTS);
		}
	}

	public void submitScore(String id, double score){
		if (isGooglePlayConnected()){
			Games.Leaderboards.submitScore(mGoogleApiClient, id, (long)(score));
		}
	}

	public void showLeaderboard(String id){
		if (isGooglePlayConnected()){
			startActivityForResult(Games.Leaderboards.getLeaderboardIntent(mGoogleApiClient, id), REQUEST_LEADERBOARD);
		}
	}

	public void showAllLeaderboards(){
		if (isGooglePlayConnected()){
			startActivityForResult(Games.Leaderboards.getAllLeaderboardsIntent(mGoogleApiClient), REQUEST_ALL_LEADERBOARD);
		}
	}

	@Override
	public void onConnected(Bundle connectionHint){
		googleConnected = true;
		mGoogleApiClient.connect();
	}

	@Override
	public void onConnectionSuspended(int i) {
		if (isOnline()){
			mGoogleApiClient.connect();
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult){
		if (connectionResult.hasResolution()) {
			try {
				connectionResult.startResolutionForResult(this, RC_SIGN_IN);
			} catch (IntentSender.SendIntentException e) {
				mGoogleApiClient.connect();
			}
		}else{
			int errorCode = connectionResult.getErrorCode();
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(errorCode, this, RC_SIGN_IN);
			if (dialog != null) {
				dialog.show();
			}
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (isGooglePlayConnected()){
			mGoogleApiClient.disconnect();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		if (hasBanner == true) {
			adView.destroy();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();

		if (hasBanner == true) {
			adView.pause();
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		if (hasBanner == true) {
			adView.resume();
		}
	}

}
