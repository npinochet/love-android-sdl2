/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "android.h"

#ifdef LOVE_ANDROID

#include "SDL.h"
#include "jni.h"

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>

namespace love
{
namespace android
{

void setImmersive(bool immersive_active)
{
	JNIEnv *env = (JNIEnv*) SDL_AndroidGetJNIEnv();

	jobject activity = (jobject) SDL_AndroidGetActivity();

	jclass clazz(env->GetObjectClass(activity));
	jmethodID method_id = env->GetMethodID(clazz, "setImmersiveMode", "(Z)V");

	env->CallVoidMethod(activity, method_id, immersive_active);

	env->DeleteLocalRef(activity);
	env->DeleteLocalRef(clazz);
}

bool getImmersive()
{
	JNIEnv *env = (JNIEnv*) SDL_AndroidGetJNIEnv();

	jobject activity = (jobject) SDL_AndroidGetActivity();

	jclass clazz(env->GetObjectClass(activity));
	jmethodID method_id = env->GetMethodID(clazz, "getImmersiveMode", "()Z");

	jboolean immersive_active = env->CallBooleanMethod(activity, method_id);

	env->DeleteLocalRef(activity);
	env->DeleteLocalRef(clazz);

	return immersive_active;
}

double getScreenScale()
{
	static double result = -1.;

	if (result == -1.)
	{
		JNIEnv *env = (JNIEnv*) SDL_AndroidGetJNIEnv();

		jobject activity = (jobject) SDL_AndroidGetActivity();
		jclass clazz (env->GetObjectClass(activity));

		jmethodID getMetrics = env->GetStaticMethodID(clazz, "getMetrics", "()Landroid/util/DisplayMetrics;");
		jobject metrics = env->CallStaticObjectMethod(clazz, getMetrics);
		jclass metricsClass = env->GetObjectClass(metrics);

		result = env->GetFloatField(metrics, env->GetFieldID(metricsClass, "density", "F"));

		env->DeleteLocalRef(metricsClass);
		env->DeleteLocalRef(metrics);
		env->DeleteLocalRef(activity);
		env->DeleteLocalRef(clazz);
	}

	return result;
}

const char *getSelectedGameFile()
{
	static const char *path = NULL;

	if (path)
	{
		delete path;
		path = NULL;
	}

	JNIEnv *env = (JNIEnv*) SDL_AndroidGetJNIEnv();

	jobject activity = (jobject) SDL_AndroidGetActivity();
	jclass clazz (env->GetObjectClass(activity));

	jmethodID getGamePath = env->GetStaticMethodID(clazz, "getGamePath", "()Ljava/lang/String;");
	jstring gamePath = (jstring) env->CallStaticObjectMethod(clazz, getGamePath);
	const char *utf = env->GetStringUTFChars(gamePath, 0);
	if (utf)
	{
		path = SDL_strdup(utf);
		env->ReleaseStringUTFChars(gamePath, utf);
	}

	env->DeleteLocalRef(gamePath);
	env->DeleteLocalRef(activity);
	env->DeleteLocalRef(clazz);

	return path;
}

bool openURL(const std::string &url)
{
	JNIEnv *env = (JNIEnv*) SDL_AndroidGetJNIEnv();

	jobject activity = (jobject) SDL_AndroidGetActivity();
	jclass clazz (env->GetObjectClass(activity));

	jmethodID openURL= env->GetStaticMethodID(clazz, "openURL", "(Ljava/lang/String;)V");
	jstring url_jstring = (jstring) env->NewStringUTF(url.c_str());

	env->CallStaticVoidMethod(clazz, openURL, url_jstring);

	env->DeleteLocalRef(url_jstring);
	env->DeleteLocalRef(activity);
	env->DeleteLocalRef(clazz);
	return true;
}

void createBanner(const std::string &ad_id, const std::string &side, const std::string &size)
{
	JNIEnv *env = (JNIEnv*) SDL_AndroidGetJNIEnv();

	jobject activity = (jobject) SDL_AndroidGetActivity();
	jclass clazz (env->GetObjectClass(activity));

	jmethodID method_id = env->GetMethodID(clazz, "createBanner", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");
	jstring ad_id_jstring = (jstring) env->NewStringUTF(ad_id.c_str());
	jstring side_jstring = (jstring) env->NewStringUTF(side.c_str());
	jstring size_jstring = (jstring) env->NewStringUTF(size.c_str());

	env->CallVoidMethod(activity, method_id, ad_id_jstring, side_jstring, size_jstring);

	env->DeleteLocalRef(ad_id_jstring);
	env->DeleteLocalRef(side_jstring);
	env->DeleteLocalRef(size_jstring);
	env->DeleteLocalRef(activity);
	env->DeleteLocalRef(clazz);
}

void createInterstitial(const std::string &ad_id)
{
	JNIEnv *env = (JNIEnv*) SDL_AndroidGetJNIEnv();

	jobject activity = (jobject) SDL_AndroidGetActivity();
	jclass clazz (env->GetObjectClass(activity));

	jmethodID method_id = env->GetMethodID(clazz, "createInterstitial", "(Ljava/lang/String;)V");
	jstring ad_id_jstring = (jstring) env->NewStringUTF(ad_id.c_str());

	env->CallVoidMethod(activity, method_id, ad_id_jstring);

	env->DeleteLocalRef(ad_id_jstring);
	env->DeleteLocalRef(activity);
	env->DeleteLocalRef(clazz);
}

void showBanner()
{
	JNIEnv *env = (JNIEnv*) SDL_AndroidGetJNIEnv();

	jobject activity = (jobject) SDL_AndroidGetActivity();
	jclass clazz (env->GetObjectClass(activity));

	jmethodID method_id = env->GetMethodID(clazz, "showBanner", "()V");

	env->CallVoidMethod(activity, method_id);

	env->DeleteLocalRef(activity);
	env->DeleteLocalRef(clazz);
}

void hideBanner()
{
	JNIEnv *env = (JNIEnv*) SDL_AndroidGetJNIEnv();

	jobject activity = (jobject) SDL_AndroidGetActivity();
	jclass clazz (env->GetObjectClass(activity));

	jmethodID method_id = env->GetMethodID(clazz, "hideBanner", "()V");

	env->CallVoidMethod(activity, method_id);

	env->DeleteLocalRef(activity);
	env->DeleteLocalRef(clazz);
}

bool isInterstitialLoaded()
{
	JNIEnv *env = (JNIEnv*) SDL_AndroidGetJNIEnv();

	jobject activity = (jobject) SDL_AndroidGetActivity();
	jclass clazz (env->GetObjectClass(activity));

	jmethodID method_id = env->GetMethodID(clazz, "isInterstitialLoaded", "()Z");

	jboolean adLoaded = env->CallBooleanMethod(activity, method_id);

	env->DeleteLocalRef(activity);
	env->DeleteLocalRef(clazz);

	return adLoaded;
}

void showInterstitial()
{
	JNIEnv *env = (JNIEnv*) SDL_AndroidGetJNIEnv();

	jobject activity = (jobject) SDL_AndroidGetActivity();
	jclass clazz (env->GetObjectClass(activity));

	jmethodID method_id = env->GetMethodID(clazz, "showInterstitial", "()V");

	env->CallVoidMethod(activity, method_id);

	env->DeleteLocalRef(activity);
	env->DeleteLocalRef(clazz);
}

void googlePlayConnect()
{
	JNIEnv *env = (JNIEnv*) SDL_AndroidGetJNIEnv();

	jobject activity = (jobject) SDL_AndroidGetActivity();
	jclass clazz (env->GetObjectClass(activity));

	jmethodID method_id = env->GetMethodID(clazz, "googlePlayConnect", "()V");

	env->CallVoidMethod(activity, method_id);

	env->DeleteLocalRef(activity);
	env->DeleteLocalRef(clazz);
}

void googlePlayDisconnect()
{
	JNIEnv *env = (JNIEnv*) SDL_AndroidGetJNIEnv();

	jobject activity = (jobject) SDL_AndroidGetActivity();
	jclass clazz (env->GetObjectClass(activity));

	jmethodID method_id = env->GetMethodID(clazz, "googlePlayDisconnect", "()V");

	env->CallVoidMethod(activity, method_id);

	env->DeleteLocalRef(activity);
	env->DeleteLocalRef(clazz);
}

bool isGooglePlayConnected()
{
	JNIEnv *env = (JNIEnv*) SDL_AndroidGetJNIEnv();

	jobject activity = (jobject) SDL_AndroidGetActivity();
	jclass clazz (env->GetObjectClass(activity));

	jmethodID method_id = env->GetMethodID(clazz, "isGooglePlayConnected", "()Z");

	jboolean isConnected = env->CallBooleanMethod(activity, method_id);

	env->DeleteLocalRef(activity);
	env->DeleteLocalRef(clazz);

	return isConnected;
}

void unlockAchievement(const std::string &id)
{
	JNIEnv *env = (JNIEnv*) SDL_AndroidGetJNIEnv();

	jobject activity = (jobject) SDL_AndroidGetActivity();
	jclass clazz (env->GetObjectClass(activity));

	jmethodID method_id = env->GetMethodID(clazz, "unlockAchievement", "(Ljava/lang/String;)V");
	jstring id_jstring = (jstring) env->NewStringUTF(id.c_str());

	env->CallVoidMethod(activity, method_id, id_jstring);

	env->DeleteLocalRef(id_jstring);
	env->DeleteLocalRef(activity);
	env->DeleteLocalRef(clazz);
}

void incrementAchievement(const std::string &id, int inc)
{
	JNIEnv *env = (JNIEnv*) SDL_AndroidGetJNIEnv();

	jobject activity = (jobject) SDL_AndroidGetActivity();
	jclass clazz (env->GetObjectClass(activity));

	jmethodID method_id = env->GetMethodID(clazz, "incrementAchievement", "(Ljava/lang/String;I)V");
	jstring id_jstring = (jstring) env->NewStringUTF(id.c_str());

	env->CallVoidMethod(activity, method_id, id_jstring, inc);

	env->DeleteLocalRef(id_jstring);
	env->DeleteLocalRef(activity);
	env->DeleteLocalRef(clazz);
}

void showAchievements()
{
	JNIEnv *env = (JNIEnv*) SDL_AndroidGetJNIEnv();

	jobject activity = (jobject) SDL_AndroidGetActivity();
	jclass clazz (env->GetObjectClass(activity));

	jmethodID method_id = env->GetMethodID(clazz, "showAchievements", "()V");

	env->CallVoidMethod(activity, method_id);

	env->DeleteLocalRef(activity);
	env->DeleteLocalRef(clazz);
}

void submitScore(const std::string &id, double score)
{
	JNIEnv *env = (JNIEnv*) SDL_AndroidGetJNIEnv();

	jobject activity = (jobject) SDL_AndroidGetActivity();
	jclass clazz (env->GetObjectClass(activity));

	jmethodID method_id = env->GetMethodID(clazz, "submitScore", "(Ljava/lang/String;D)V");
	jstring id_jstring = (jstring) env->NewStringUTF(id.c_str());

	env->CallVoidMethod(activity, method_id, id_jstring, score);

	env->DeleteLocalRef(id_jstring);
	env->DeleteLocalRef(activity);
	env->DeleteLocalRef(clazz);
}

void showLeaderboard(const std::string &id)
{
	JNIEnv *env = (JNIEnv*) SDL_AndroidGetJNIEnv();

	jobject activity = (jobject) SDL_AndroidGetActivity();
	jclass clazz (env->GetObjectClass(activity));

	jmethodID method_id = env->GetMethodID(clazz, "showLeaderboard", "(Ljava/lang/String;)V");
	jstring id_jstring = (jstring) env->NewStringUTF(id.c_str());

	env->CallVoidMethod(activity, method_id, id_jstring);

	env->DeleteLocalRef(id_jstring);
	env->DeleteLocalRef(activity);
	env->DeleteLocalRef(clazz);
}

void showAllLeaderboards()
{
	JNIEnv *env = (JNIEnv*) SDL_AndroidGetJNIEnv();

	jobject activity = (jobject) SDL_AndroidGetActivity();
	jclass clazz (env->GetObjectClass(activity));

	jmethodID method_id = env->GetMethodID(clazz, "showAllLeaderboards", "()V");

	env->CallVoidMethod(activity, method_id);

	env->DeleteLocalRef(activity);
	env->DeleteLocalRef(clazz);
}

void vibrate(double seconds)
{
	JNIEnv *env = (JNIEnv*) SDL_AndroidGetJNIEnv();

	jobject activity = (jobject) SDL_AndroidGetActivity();
	jclass clazz (env->GetObjectClass(activity));

	jmethodID vibrate_method = env->GetStaticMethodID(clazz, "vibrate", "(D)V");
	env->CallStaticVoidMethod(clazz, vibrate_method, seconds);

	env->DeleteLocalRef(activity);
	env->DeleteLocalRef(clazz);
}

/*
 * Helper functions for the filesystem module
 */
void freeGameArchiveMemory(void *ptr)
{
	char *game_love_data = static_cast<char*>(ptr);
	delete[] game_love_data;
}

bool loadGameArchiveToMemory(const char* filename, char **ptr, size_t *size)
{
	SDL_RWops *asset_game_file = SDL_RWFromFile(filename, "rb");
	if (!asset_game_file) {
		SDL_Log("Could not find %s", filename);
		return false;
	}

	Sint64 file_size = asset_game_file->size(asset_game_file);
	if (file_size <= 0) {
		SDL_Log("Could not load game from %s. File has invalid file size: %d.", filename, (int) file_size);
		return false;
	}

	*ptr = new char[file_size];
	if (!*ptr) {
		SDL_Log("Could not allocate memory for in-memory game archive");
		return false;
	}

	size_t bytes_copied = asset_game_file->read(asset_game_file, (void*) *ptr, sizeof(char), (size_t) file_size);
	if (bytes_copied != file_size) {
		SDL_Log("Incomplete copy of in-memory game archive!");
		delete[] *ptr;
		return false;
	}

	*size = (size_t) file_size;
	return true;
}

bool directoryExists(const char *path)
{
	struct stat s;
	int err = stat(path, &s);
	if (err == -1)
	{
		if (errno != ENOENT)
			SDL_Log("Error checking for directory %s errno = %d: %s", path, errno, strerror(errno));
		return false;
	}

	return S_ISDIR(s.st_mode);
}

bool mkdir(const char *path)
{
	int err = ::mkdir(path, 0770);
	if (err == -1)
	{
		SDL_Log("Error: Could not create directory %s", path);
		return false;
	}

	return true;
}

bool createStorageDirectories()
{
	std::string internal_storage_path = SDL_AndroidGetInternalStoragePath();

	std::string save_directory = internal_storage_path + "/save";
	if (!directoryExists(save_directory.c_str()) && !mkdir(save_directory.c_str()))
		return false;

	std::string game_directory = internal_storage_path + "/game";
	if (!directoryExists (game_directory.c_str()) && !mkdir(game_directory.c_str()))
		return false;

	return true;
}

} // android
} // love

#endif // LOVE_ANDROID
